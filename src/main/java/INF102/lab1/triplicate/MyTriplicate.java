package INF102.lab1.triplicate;

import java.util.List;
import java.util.HashMap;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {

        var counter = new HashMap<T,Integer>();

        for (var item : list)
        {
            var count = counter.getOrDefault(item, 0) + 1;

            if (count == 3) return item;

            counter.put(item, count + 1);
        }

        return null;
    }
    
}